# D15Crypt 1.2

Strong multi-threaded encryption and decryption written in Rust.

- Ability to set custom number of threads to use
- Also passes a users' password to a derivation function to increase the strength of the password
- 4 levels of encryption strength and speeds

For help on usage run `d15crypt -h`
