use d15crypt::structs;
use d15crypt::globals;

extern {
    // fn foo_function();
    // fn bar_function(x: i32) -> i32;
    //fn _my_adder(a: &[u8;2], b: u64) -> u64;
    fn _my_adder(a: u8, b: u8) -> u64;
}

pub fn call() {
    unsafe {
        // foo_function();
        // 1println!("Answer:{}",bar_function(42));
        //println!("Answer:{}", _my_adder(&array,2))
        println!("Answer:{}", _my_adder(5,2))
    }
}



fn main() {
    //call();
    //let t = structs::RandomSubstitutionFields::new();

    let args_array: Vec<String> = std::env::args().collect::<Vec<String>>();
    let mut cli_args = rust_cli_arguments::Arguments::new();
    let result_cli_args = cli_args.get_from_command_line();
    if result_cli_args.is_ok() {
        let cli_args = result_cli_args.unwrap();
        let version_info: String = "1.2.0".into();
        if cli_args.get_argument_value("-h").is_some() || cli_args.get_argument_value("--help").is_some() {
            show_help_screen(&version_info);
            return;
        }
        let option_version = cli_args.get_argument_value("-v");
        if option_version.is_some() {
            println!("D15Crypt version {}", version_info);
            println!("Command-line argument '-h' for help");
            return;
        }
        let option_crypt_version = cli_args.get_argument_value("-x");
        let option_file_name = cli_args.get_argument_value("-i");
        let option_password = cli_args.get_argument_value("-p");
        let option_output_file_name = cli_args.get_argument_value("-o");
        let option_decrypt = cli_args.get_argument_value("-d");
        let option_speedtest = cli_args.get_argument_value("-t");
        let result_concurrency = globals::get_concurrency_number(cli_args.get_argument_value("-c"));
        let option_concurrency = match result_concurrency.is_ok() {
            true => result_concurrency.unwrap(),
            _ => {
                println!("{}", result_concurrency.unwrap_err());
                println!("Command-line argument '-h' for help");
                return;
            }
        };

        if option_speedtest.is_some() { // is argument -t present? If so do a benchmark run
            globals::run_speed_test(option_crypt_version, option_concurrency, &version_info);
        } else {
            if option_file_name.is_some() && option_password.is_some() {
                let file_name = option_file_name.unwrap();
                let option_file_bytes = d15crypt::files::read_from_file(&file_name); // Read all the bytes from the input file
                if option_file_bytes.is_some() {
                    let crypt_version: u16 = d15crypt::globals::get_encryption_version_number(option_crypt_version);
                    let password: Vec<u8> = option_password.unwrap().as_bytes().to_vec();
                    let mut to_crypt = option_file_bytes.unwrap();
                    let to_crypt_length = to_crypt.len();
                    if file_name.ends_with(".xd15") == false && option_decrypt.is_none() { // If a files' extension does not end with ".xd15" then treat the needed process as an encryption of the file... not a decryption of the file
                        let output_file_name = match option_output_file_name.is_some() {
                            true => option_output_file_name.unwrap(),
                            _ => format!("{}.xd15", d15crypt::files::get_only_file_name(&file_name))
                        };
                        let start = std::time::Instant::now();
                        let result_encrypted = d15crypt::encrypt(&crypt_version, &mut to_crypt, &password, option_concurrency);
                        let elapsed = std::time::Duration::from_millis(start.elapsed().as_millis() as u64);
                        if result_encrypted.is_err() {
                            println!("{}", result_encrypted.unwrap_err());
                        } else {
                            println!("Encrypted at {:.2}MiB/s", (to_crypt_length as f32 / (elapsed.as_millis() as f32) / 1024_f32));
                            d15crypt::files::write_to_file(&output_file_name, &result_encrypted.clone().unwrap(), false);
                        }
                    } else {
                        // Do a decryption of the given of a file
                        let only_file_name = d15crypt::files::get_only_file_name(&file_name);
                        let output_file_name = match option_output_file_name.is_some() {
                            true => format!("{}", option_output_file_name.unwrap()),
                            _ => format!("{}", match only_file_name.ends_with(".xd15") {
                                true => String::from(only_file_name.strip_suffix(".xd15").unwrap()),
                                _ => only_file_name
                            })
                        };
                        let st = structs::StopWatch::new();
                        let result_decrypted = d15crypt::decrypt(&mut to_crypt, &password, option_concurrency);
                        let elapsed = std::time::Duration::from_millis(st.elapsed_milliseconds() as u64);
                        if result_decrypted.is_err() {
                            println!("{}", result_decrypted.unwrap_err());
                        } else {
                            println!("Decrypted at {:.2}MiB/s", (to_crypt_length as f32 / (elapsed.as_millis() as f32) / 1024.0));
                            let extracted_file_name: &str = match output_file_name.ends_with(".xd15") {
                                true => output_file_name.strip_suffix(".xd15").unwrap(),
                                _ => &output_file_name
                            };
                            d15crypt::files::write_to_file(extracted_file_name, &result_decrypted.unwrap(), false);
                        }
                    }
                } else {
                    println!("Could not read file: {}", args_array[1]);
                }
            } else {
                if option_file_name.is_none() {
                    println!("Specify a file to encrypt using '-i'");
                }
                if option_password.is_none() {
                    println!("Specify a password using '-p'");
                }
                println!("Command-line argument '-h' for help");
            }
        }
    } else {
        println!("Error reading command line arguments");
        println!();
        println!("Command-line argument '-h' for help");
    }
}


fn show_help_screen(version_info: &str) {
    println!(" ____  _ ____   ____                  _   ");
    println!("|  _ \\/ | ___| / ___|_ __ _   _ _ __ | |_ ");
    println!("| | | | |___ \\| |   | '__| | | | '_ \\| __|");
    println!("| |_| | |___) | |___| |  | |_| | |_) | |_ ");
    println!("|____/|_|____/ \\____|_|   \\__, | .__/ \\__|");
    println!("                          |___/|_|        ");
    println!("D15Crypt version {}", version_info);
    println!("D15Crypt is free to use, modify and redistribute");
    println!("Licence: Dimension15 GPL v1 - Similar to GNU GPL v3");
    println!();
    println!("Encryption and decryption of files using multiple threads");
    println!();
    println!("METRICS");
    println!("    Version  Rounds  password len(bytes)  block len(bytes)");
    println!("    1.0      6       15 - 255             256");
    println!("    1.1      12      15 - 511             512");
    println!("    1.2      16      15 - 1023            1024");
    println!("    1.3      20      15 - 4095            4096");
    println!();
    println!("ARGUMENTS");
    println!("    -i    Input file path");
    println!("    -p    Encryption/decryption password");
    println!("    -x    (Optional) Encryption version (10, 11, 12, 13), default is version 1.1 (11)");
    println!("          Only needed when doing an encryption, not when decrypting");
    println!("    -o    (Optional) Path for output file");
    println!("    -c    (Optional) Concurrent threads to use");
    println!("          A default concurrency value (== number of physical cores) of greater than 1 is only applied to files larger than 256KiB");
    println!("    -d    (Optional) Force a decryption. Use when input file does not have extension '.xd15'");
    println!("    -t    (Optional) Run an encryption speed benchmark, all other arguments are ignored");
    println!();
    println!("    -v    Print version information");
    println!("    -h    Print this help screen");
    println!();
    println!("USAGE");
    println!("    Encrypt a file");
    println!("        d15crypt -i [file name] -p [password] [-x [version number]] [-o [output file name]]");
    println!("        Examples:");
    println!("            - Output file being auto named SecretDocument.pdf.xd15 and default version being 1.1");
    println!("                d15crypt -i SecretDocument.pdf -p 45MySecretpassword12345");
    println!("            - Output file being named private.enc and version being 1.2");
    println!("                d15crypt -i SecretDocument.pdf -p \"&$gv5w34RG4346rgSDFH%@\" -x 12 -o private.enc");
    println!("    Decrypt a file");
    println!("        d15crypt -i [path] -p [password] [-o [output file name]] [-d]");
    println!("            - Output file being auto named SecretDocument.pdf");
    println!("                d15crypt -i SecretDocument.pdf.xd15 -p 45MySecretpassword12345");
    println!("            - Output file being named ExtractedSecretDocument.pdf");
    println!("                d15crypt -i private.enc -p \"&$gv5w34RG4346rgSDFH%@\" -d -o ExtractedSecretDocument.pdf");
    println!();
}