use std::usize;

pub mod globals;
pub mod structs;
pub mod serialisation;
pub mod files;
mod testing;

/// Stores the data type descriptor
const HEADER_DATA_TYPE: usize = 16;
// Stores version number
const HEADER_VERSION: usize = 2;
/// Stores number of threads to use
const HEADER_CONCURRENCY: usize = 2;
/// Stores original bytes length
const HEADER_DATA_LENGTH: usize = 4;
/// The complete length of the header
const HEADER_LENGTH: usize = HEADER_DATA_TYPE + HEADER_VERSION + HEADER_DATA_LENGTH + HEADER_CONCURRENCY;
/// Concurrency (threads) of more than 1 is allowed if the number of bytes to encrypt/decrypt is greater than this number in KiB
const ALLOW_CONCURRENCY_FROM_KIB: usize = 256;

pub fn substitute_bytes_from_field(to_substitute: &mut Vec<u8>, password: &Vec<u8>, encrypting: bool) {
    let substitution_fields = structs::RandomSubstitutionFields::new();
    let substitution_field: Vec<u8> = substitution_fields.get_field_from_password(encrypting, password);
    for index in 0..to_substitute.len() {
        to_substitute[index] = substitution_field[to_substitute[index] as usize] as u8;
     };
}

///Encrypt a vector according to an encryption version
pub fn encrypt(version: &u16, to_encrypt: &mut Vec<u8>, password: &Vec<u8>, concurrency: Option<usize>) -> Result<Vec<u8>, String> {
    substitute_bytes_from_field(to_encrypt, password, true);

    let metrics: structs::Metrics = structs::Metrics::from_version(version.to_owned())?;

    let seed: Vec<u8> = globals::generate_random_vector(&(metrics.block_len));
    let derived_password = globals::generate_derived_password(&password, &seed, &metrics)?;
    structs::ProcessedBlocks::encrypt(metrics, to_encrypt, &derived_password, &seed, concurrency)
    //structs::ProcessedBlocks::encrypt(metrics, to_encrypt, &password, &seed, concurrency)
}


///Decrypt a given byte vector
pub fn decrypt(to_decrypt: &mut Vec<u8>, password: &Vec<u8>, option_concurrency: Option<usize>) -> Result<Vec<u8>, String> {
    let header_extraction_error_message: String = String::from("Could not extract header data from supplied bytes");

    return if to_decrypt.len() > HEADER_LENGTH { //larger than bytes for file type, version and length of original bytes
        let mut deserialiser = serialisation::Deserialise::new(to_decrypt, "Decryption");
        let data_type = deserialiser.extract_field(HEADER_DATA_TYPE)?;
        if data_type == "D15ENCV1.2      ".as_bytes().to_vec() {
            let metrics: structs::Metrics = structs::Metrics::from_bytes(&deserialiser.extract_field(HEADER_VERSION)?)?;
            let _concurrency = globals::get_16bit_number_from(&deserialiser.extract_field(HEADER_CONCURRENCY)?)?;
            let original_bytes_len = globals::get_32bit_number_from(&deserialiser.extract_field(HEADER_DATA_LENGTH)?)?;
            let _ : Vec<u8> = to_decrypt.drain(..HEADER_LENGTH).collect();
            let seed: Vec<u8> = to_decrypt.drain(..metrics.block_len as usize).collect();
            let derived_password = globals::generate_derived_password(&password, &seed, &metrics)?;

            let obfuscated_blocks: Vec<Vec<u8>> = structs::ProcessedBlocks::extract_task_blocks(to_decrypt, structs::ProcessedBlocks::get_concurrency(option_concurrency, original_bytes_len as usize));
            let mut blocks = structs::ProcessedBlocks::new();
            for block in 0..obfuscated_blocks.len() {
                blocks.add(structs::ProcessedBytesBlock::new(&obfuscated_blocks[block]));
            }
            let result_decrypted = blocks.decrypt(&metrics, &derived_password, &seed);
            if result_decrypted.is_ok() {
                let mut decrypted = result_decrypted.unwrap();
                println!("{}", decrypted.len()/1024/1024);
                substitute_bytes_from_field(&mut decrypted, password, false);
                Ok(decrypted[0..original_bytes_len as usize].to_vec())
            } else {
                Err(header_extraction_error_message)
            }
        } else {
            Err("Not a recognised encrypted file format".into())
        }
    } else {
        Err(header_extraction_error_message)
    }
}