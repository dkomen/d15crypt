#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use crate::*;

    #[test]
    fn encrypt() {
        // This assert would fire and test will fail.
        // Please note, that private functions can be tested too!
        let crypt_version: u16 = globals::get_encryption_version_number(Some("11".into()));
        let to_crypt: Vec<u8> = globals::generate_random_vector_in_mib(10);
        let result: Result<Vec<u8>, String> = Ok(to_crypt);
        let result_result = crate::encrypt(&crypt_version, &mut result.unwrap(), &vec![49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49], None);
        if result_result.is_ok() {
            let result = result_result.unwrap();
            println!("{:?}", result[0..1152].to_vec());
            assert_eq!(result[0..20], [68, 49, 53, 69, 78, 67, 86, 49, 46, 50, 32, 32, 32, 32, 32, 32, 0, 11, 0, 31]);
        } else {
            eprintln!("Error: {}", result_result.unwrap_err());
        }
    }
}