use super::globals;
/// Methods to be used to serialise a struct
pub struct Serialise {
    serialised_fields: Vec<u8>,
    /// A description to show in errors
    description: String
}
impl Serialise {
    pub fn new(description: &str) -> Serialise {
        Serialise {
            serialised_fields: vec![],
            description: description.into()
        }
    }

    /// A padded_length value of 0 means to do no padding and just use the bytes as is
    pub fn add_field(&mut self, mut bytes: &mut Vec<u8>, padded_length: usize)-> Result<(), String> {
        if bytes.len() < padded_length {
            bytes = globals::append_vector_to_length(bytes, padded_length);
            self.serialised_fields.append(&mut bytes);
            return Ok(())
        } else if bytes.len() == padded_length || padded_length == 0 {
            self.serialised_fields.append(&mut bytes);
            return Ok(());
        }

        Err(format!("Serialisation error: {}", self.description))
    }

    pub fn into_bytes(&self) -> Vec<u8> {
        let mut length = (self.serialised_fields.len() + globals::LENGTH_OF_U64_NUMBER_IN_BYTES).to_be_bytes().to_vec();
        let mut new_vec: Vec<u8> = vec![];

        new_vec.append(&mut length);
        new_vec.extend(self.serialised_fields.iter());

        new_vec
    }
}

/// Methods to be used to deserialise a struct
pub struct Deserialise {
    serialised_fields: Vec<u8>,
    current_field_pointer: usize,
    /// A description to show in errors
    description: String
}
impl Deserialise {
    pub fn new(serialised_fields: &Vec<u8>, description: &str) -> Deserialise {
        Deserialise {
            serialised_fields: serialised_fields.to_owned(),
            current_field_pointer: 0,
            description: description.into()
        }
    }

    pub fn extract_field(&mut self, padded_length: usize)-> Result<Vec<u8>, String> {
        if self.current_field_pointer + padded_length <= self.serialised_fields.len() {
            let extracted_field: Vec<u8> = self.serialised_fields[self.current_field_pointer..self.current_field_pointer + padded_length].to_vec();
            self.current_field_pointer += padded_length;

            Ok(extracted_field)
        } else {
            Err(format!("Deserialisation error: {} - bytes: {}, current pointer: {}, length of current field: {}", self.description, self.serialised_fields.len(), self.current_field_pointer, padded_length))
        }
    }

    pub fn extract_field_to_end(&mut self)-> Result<Vec<u8>, String> {
        if self.current_field_pointer <= self.serialised_fields.len() {
            let extracted_field: Vec<u8> = self.serialised_fields[self.current_field_pointer..self.current_field_pointer + (self.serialised_fields.len() - self.current_field_pointer)].to_vec();

            Ok(extracted_field)
        } else {

            Err(format!("Deserialisation error: {}", self.description))
        }
    }
}