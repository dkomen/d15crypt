use std::fs::File;
use std::io::prelude::*;

/// Strip directory name form file path, leaving only file name
pub fn get_only_file_name(path: &str) -> String {
    let path_buff = std::path::PathBuf::from(path);

    path_buff.components().last().unwrap().as_os_str().to_string_lossy().into()
}

/// Read a files contents and return it as a vector
pub fn read_from_file(path: &str) -> Option<Vec<u8>> {
    let mut file_data: Vec<u8> = vec![];

    let file_handle = File::open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(error) =>  {println!("Error opening file: {}", error); return None },
    };

    let results = file_handle.read_to_end(&mut file_data,);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {println!("Error reading from file locally: {}", error); return None },
    };

    Some(file_data)
}

/// Write a vector to a file
pub fn write_to_file(path: &str, file_data: &Vec<u8>, append_to_file: bool) -> bool {
    if !append_to_file && path_exists(&path) {
        let _ = delete_file(&path);
    }
    let file_handle = std::fs::OpenOptions::new().create(true).write(true).append(append_to_file).open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(error) => {
            println!("Error creating the file '{}': Error:{:?}", path, error);
            return false;
        }
    };

    let results = file_handle.write(&file_data);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {
            println!("Error writing file '{}': {:?}", path, error);
            return false;
        }
    };

    true
}

/// Delete a file from disk
pub fn delete_file(file_path: &str) -> bool {
    if path_exists(&file_path) {
        if std::fs::remove_file(&file_path).is_ok() {
            true
        } else {
            println!("Could not delete file: {}", file_path);

            false
        }
    } else {
        // File doesn't exist, so a delete success is reported
        true
    }
}

/// Does a file exist?
pub fn path_exists(path: &str) -> bool {
    let path_to_file_buff = std::path::PathBuf::from(path);
    let path_to_file = path_to_file_buff.to_str().unwrap();

    std::path::Path::new(path_to_file).exists()
}